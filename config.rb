Encoding.default_external = "utf-8"

# Require any additional compass plugins here.
require 'sass-globbing'
require 'autoprefixer-rails'
require 'csso'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "public/css"
images_dir = "public/img"
sass_dir = "sass"
javascripts_dir = "js"
# Browser support
support = [ "> 3%"]
# csso is a special optimizer, much better than default
# output_style = :csso
# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

on_stylesheet_saved do |file|
  css = File.read(file)
  File.open(file, 'w') do |io|
  	# Prefixed only
  	if output_style == :csso
  		io << Csso.optimize(AutoprefixerRails.process(css, browsers: support).css)	
  	else
  		io << AutoprefixerRails.process(css, browsers: support)
  	end
  end
end